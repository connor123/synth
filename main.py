from PyQt5 import QtGui
from custom_gui import Slider, Dial, set_color
from PyQt5.QtCore import Qt, pyqtSignal
from waveforms.triangle import TriangleBlock
from waveforms.sine import SineBlock
from waveforms.saw import SawBlock
from PyQt5.QtWidgets import QApplication, QWidget, QHBoxLayout, QVBoxLayout, QDial, QLabel, QLCDNumber
import time
import random
app = QApplication([])
from pyo import *

font_size = 30
block_width = 150

s = Server()
output = pa_get_default_output() 
s.setInOutDevice(output)
s.boot().start()
s.amp=0.1

# create background widget
top_win = QWidget() 
set_color(top_win, Qt.cyan)
top_win.setAutoFillBackground(True)
top_win.show() 
top_win.resize(1920,1080)
top_win.setWindowTitle("Synth-01") 
top_vlayout = QVBoxLayout()
top_win.setLayout(top_vlayout)


# create main widgets
mod_group = QWidget()
mod_group.setMaximumWidth(1400)
audio_boxes = QWidget()
audio_boxes.setMaximumWidth(1400)
keyboard = QWidget()
keyboard.setMaximumWidth(1400)
audio_layout = QHBoxLayout()
top_vlayout.addWidget(mod_group)
top_vlayout.addWidget(audio_boxes)
top_vlayout.addWidget(keyboard)

set_color(audio_boxes, Qt.green)
set_color(keyboard, Qt.red)
set_color(mod_group, Qt.gray)
mod_layout = QHBoxLayout()
mod_group.setAutoFillBackground(True)
mod_group.setLayout(mod_layout)

audio_layout = QHBoxLayout()
audio_boxes.setAutoFillBackground(True)
audio_boxes.setLayout(audio_layout)

keyboard_layout = QHBoxLayout()
keyboard.setAutoFillBackground(True)
keyboard.setLayout(keyboard_layout)




class AmpBlock(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.setLayout(QVBoxLayout())
        self.setMaximumWidth(190)
        self.freq_screen = QLCDNumber()
        dial = Dial(self, min_val=0, max_val=100, default=0)
        newfont = QtGui.QFont("Times", font_size, QtGui.QFont.Bold) 
        label = QLabel("Amplifier")
        label.setFont(newfont)
        set_color(self, Qt.green)
        self.layout().addWidget(label)
        self.layout().addWidget(self.freq_screen)
        self.layout().addWidget(dial)
        dial.valueChanged.connect(self.onValueChanged)
    def onValueChanged(self, value):
        dirstr = "Forward" if self.sender().direction() == Slider.Forward else "Backward"
        if dirstr == "Forward":
            print(value, dirstr)
            s.setAmp(value/100)
            self.freq_screen.display(s.amp)
        if dirstr == "Backward":
            print(value, dirstr)
            s.setAmp(value/100)
            self.freq_screen.display(s.amp)
            #self.sine.setFreq(self.sine.freq - 1)


class EnvelopeBlock(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.setLayout(QHBoxLayout())
        self.setMaximumWidth(block_width* 4)
        attack_label = QLabel("Attack")
        decay_label = QLabel("Decay")
        sustain_label = QLabel("Sustain")
        release_label = QLabel("Release")
        attack_widget = QWidget()
        attack_layout = QVBoxLayout()
        attack_dial = Dial(self, min_val=0, max_val=100, default=0)
        attack_widget.setLayout(attack_layout)
        attack_layout.addWidget(attack_dial)
        decay_widget = QWidget()
        decay_layout = QVBoxLayout()
        decay_dial = Dial(self, min_val=0, max_val=100, default=1)
        decay_widget.setLayout(decay_layout)
        decay_layout.addWidget(decay_dial)
        sustain_widget = QWidget()
        sustain_layout = QVBoxLayout()
        sustain_dial = Dial(self, min_val=0, max_val=5, default=1)
        sustain_widget.setLayout(sustain_layout)
        sustain_layout.addWidget(sustain_dial)
        release_widget = QWidget()
        release_layout = QVBoxLayout()
        release_dial = Dial(self, min_val=0, max_val=100, default=0)
        release_widget.setLayout(release_layout)
        release_layout.addWidget(release_dial)
        newfont = QtGui.QFont("Times", font_size, QtGui.QFont.Bold) 
        release_label.setFont(newfont)
        sustain_label.setFont(newfont)
        attack_label.setFont(newfont)
        decay_label.setFont(newfont)
        sustain_layout.addWidget(sustain_label)
        attack_layout.addWidget(attack_label)
        decay_layout.addWidget(decay_label)
        release_layout.addWidget(release_label)
        set_color(self, Qt.green)
        self.layout().addWidget(attack_widget)
        self.layout().addWidget(decay_widget)
        self.layout().addWidget(sustain_widget)
        self.layout().addWidget(release_widget)
        #dial.valueChanged.connect(self.onValueChanged)
    def onValueChanged(self, value):
        dirstr = "Forward" if self.sender().direction() == Slider.Forward else "Backward"
        if dirstr == "Forward":
            print(value, dirstr)
            s.setAmp(value/100)
            self.freq_screen.display(s.amp)
        if dirstr == "Backward":
            print(value, dirstr)
            s.setAmp(value/100)
            self.freq_screen.display(s.amp)
            #self.sine.setFreq(self.sine.freq - 1)

sineblock = SineBlock()
triangleblock = TriangleBlock()
sawblock = SawBlock()
amp = AmpBlock()
env = EnvelopeBlock()
mod_layout.addWidget(sineblock)
mod_layout.addWidget(triangleblock)
mod_layout.addWidget(sawblock)
mod_layout.addWidget(amp)
audio_layout.addWidget(env)











































# # add widgets to layout
# hlayout.addWidget(dial)

# # make it red
# p = win.palette()
# p.setColor(win.backgroundRole(), Qt.red)
# win.setPalette(p)

# class OscWidget(QWidget):
#     def __init__(self, parent=None):
#         QWidget.__init__(self, parent)
#         self.osc = Sine(freq=50, phase=0, mul=1, add=0)
#         self.osc.out()
#         self.setLayout(QVBoxLayout())
#         #slider = Slider(self)
#         # slider.setOrientation(Qt.Horizontal)
#         # self.layout().addWidget(slider)
#         # slider.directionChanged.connect(self.onDirectionChanged)
#         # slider.valueChanged.connect(self.onValueChanged)
#         dial = Dial(self)
#         dial.setOrientation(Qt.Horizontal)
#         self.layout().addWidget(dial)
#         dial.directionChanged.connect(self.onDirectionChanged)
#         dial.valueChanged.connect(self.onValueChanged)
#     def onDirectionChanged(self, direction):
#         if direction == Dial.Forward:
#             print("Forward")
#         elif direction == Dial.Backward:
#             print("Backward")
#     def onValueChanged(self, value):
#         dirstr = "Forward" if self.sender().direction() == Dial.Forward else "Backward"
#         if dirstr == "Forward":
#             print(value, dirstr)
#             self.osc.setFreq(self.osc.freq + 4)
#         if dirstr == "Backward":
#             print(value, dirstr)
#             self.osc.setFreq(self.osc.freq - 4)


# slider = OscWidget()
# hlayout.addWidget(slider)
# #slider.slider.setOrientation(Qt.Vertical)
