# import dependancies
from synth import *
from custom_gui import Slider
from PyQt5.QtCore import Qt, pyqtSignal
from PyQt5.QtWidgets import QApplication, QWidget, QHBoxLayout, QVBoxLayout, QDial, QLabel
import time
import random
# from pyo import *
# s = Server().boot()
# s.start()




class BoardWidget(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        # set the size and title
        self.resize(600,450) # resize window
        self.setWindowTitle("Synth-01") # rename title
        # create layouts
        self.hlayout = QHBoxLayout()
        self.vlayout = QVBoxLayout()
        # set layouts
        self.setLayout(self.hlayout)
        self.hlayout.addLayout(self.vlayout)
        # make it red ;)
        p = self.palette()
        p.setColor(self.backgroundRole(), Qt.red)
        self.setPalette(p)

class SineBlock(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()
    def initUI(self):
        # create controls
        slider = Slider(Qt.Horizontal)
        slider.setMinimum(15)
        slider.setMaximum(20000)
        #dial = QDial()
        # add controls to layout
        self.hlayout = QHBoxLayout
        self.hlayout.addWidget(slider)
        #self.hlayout.addWidget(dial)
        sine = Sine(freq=0)
        sine.out()
        self.show()
        slider.valueChanged.connect(self.onValueChanged)   
    def onValueChanged(self):
        sender = self.sender()
        sine.setFreq(value)
        if sender.direction() == Slider.Forward:
            value = sender.value
            print(value)
        dirstr = "Forward" if self.sender().direction() == Slider.Forward else "Backward"
        print(freq, dirstr)
        print("sine wave!")


#     server.amp = server.amp + step
    def sinewave(self, freq=100,phase=0, mul=0.9, add=0):
        try:
            sine = Sine(freq, phase, mul, add)
            sine.out()
            print("sinewave! function")
            server.amp = server.amp - step
            return sine
        except Exception as e:
            print(e)

    def change_amplitude(server, slider, step=0.1):
        direction = slider.direction() # returns 1 for forwards and 2 for backwards+        
        if direction == 1:
            print("!")
        return server


if __name__ == '__main__':
    
    app = QApplication(sys.argv)
    synth = Audioblock()
    sys.exit(app.exec_())


"""
source = control(dial or slider)
object = gui_data(direction, quantity)
target = 
"""