from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QApplication, QWidget, QHBoxLayout, QVBoxLayout, QDial, QSlider


class Slider(QSlider):
    # 0=Nothing, 1=Forward, 2=Backward
    Nothing, Forward, Backward = range(3)
    # defines a signal called directionChanged
    directionChanged = pyqtSignal(int)
    def __init__(self, parent=None):
        QSlider.__init__(self, parent)
        self._direction = Slider.Nothing
        self.last = self.value()/self.maximum()
        self.valueChanged.connect(self.onValueChanged)
    def onValueChanged(self, value):
        current = value/self.maximum()
        direction = Slider.Forward if self.last < current else Slider.Backward
        if self._direction != direction:
            self.directionChanged.emit(direction)
            self._direction = direction
        self.last = current
    def direction(self):
        return self._direction

class Dial(QDial):
    style =''' 
    QDial
    {
        background-color:QLinearGradient( 
            x1: 0.177, y1: 0.004, x2: 0.831, y2: 0.911, 
            stop: 0 white, 
            stop: 0.061 white, 
            stop: 0.066 lightgray, 
            stop: 0.5 #242424, 
            stop: 0.505 #000000,
            stop: 0.827 #040404,
            stop: 0.966 #292929,
            stop: 0.983 #2e2e2e
        );
    }
    '''
    # 0=Nothing, 1=Forward, 2=Backward
    Nothing, Forward, Backward = range(3)
    # defines a signal called directionChanged
    directionChanged = pyqtSignal(int)
    def __init__(self, parent=None, min_val=1, max_val=100, default=0):
        QDial.__init__(self, parent)
        self.setStyleSheet(self.style)
        self.setMinimum(min_val)
        self.setMaximum(max_val)
        self.setValue(default)
        self.setAutoFillBackground(True)
        self._direction = Slider.Nothing
        self.last = self.value()/self.maximum()
        self.valueChanged.connect(self.onValueChanged)
    def onValueChanged(self, value):
        current = value/self.maximum()
        direction = Slider.Forward if self.last < current else Slider.Backward
        if self._direction != direction:
            self.directionChanged.emit(direction)
            self._direction = direction
        self.last = current
    def direction(self):
        return self._direction

def set_color(widget, color):
    p = widget.palette()
    p.setColor(widget.backgroundRole(), color)
    widget.setPalette(p)
