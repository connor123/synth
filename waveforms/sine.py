from PyQt5.QtCore import Qt, pyqtSignal
from pyo import *
from PyQt5 import QtGui
from custom_gui import Slider, Dial, set_color
from PyQt5.QtWidgets import QWidget, QVBoxLayout, QLabel, QLCDNumber
class SineBlock(QWidget):
    def __init__(self, parent=None):
        QWidget.__init__(self, parent)
        self.setLayout(QVBoxLayout())
        self.setMaximumWidth(190)
        dial = Dial(self)
        newfont = QtGui.QFont("Times", 30, QtGui.QFont.Bold) 
        label = QLabel("Sine Oscillator")
        label.setFont(newfont)
        dial.setMinimum(20)
        dial.setAutoFillBackground(True)
        set_color(self, Qt.red)
        dial.setMaximum(5000)
        self.sine = Sine(freq = 1000)
        self.sine.out()
        self.freq_screen = QLCDNumber()
        # dial.setOrientation(Qt.Horizontal)
        self.layout().addWidget(label)
        self.layout().addWidget(self.freq_screen)
        self.layout().addWidget(dial)
        dial.directionChanged.connect(self.onDirectionChanged)
        dial.valueChanged.connect(self.onValueChanged)
    def onDirectionChanged(self, direction):
        if direction == Dial.Forward:
            print("Forward")
        elif direction == Dial.Backward:
            print("Backward")
    def onValueChanged(self, value):
        dirstr = "Forward" if self.sender().direction() == Slider.Forward else "Backward"
        if dirstr == "Forward":
            print(value, dirstr)
            self.sine.setFreq(value)
            self.freq_screen.display(value)
            #self.freq_screen()
        if dirstr == "Backward":
            print(value, dirstr)
            self.sine.setFreq(value)
            self.freq_screen.display(value)